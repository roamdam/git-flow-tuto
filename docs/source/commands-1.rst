
.. _commands first steps:

Basic Git commands
==================

Commiting changes
-----------------

.. code:: bash

   git add file1    # add file1 to soon-to-come commit
   git add file2    # add file2 also
   git commit -m "Update file1 and file2"

   git add file*    # add all files starting with `file`

   # Add all changed files and commit, does not include new files
   git commit -a -m "Your commit message"

   git status       # display the list of changed / added files

   # Add all files including new ones, then commit
   git add . && git commit -m "Your commit message"

   git commit -a     # No -m option will open up a text editor

   git add -p file1  # Add hunks of file1 in interactive mode

Time flexibility : reset, checkout, amend, revert and stash
-----------------------------------------------------------

.. code:: bash

   # reset the git history to the previous commit while
   # keeping the changes to files, and keeping them added
   git reset --soft HEAD~1  

   # same but changes are not added here
   git reset --mixed HEAD~1

   # changes are all cancelled /!\ DANGER /!\
   git reset --hard HEAD~1

   git reset --soft HEAD~n  # soft reset to the n-th commit backwards

   # withdraw file1 from the list of files included in the commit to come
   git reset HEAD file1

   # cancel uncommited changes to file1
   git checkout -- file1
   git checkout -- .   # cancel all uncommited changes

   git commit --amend  # rewrite the last commit

   # revert previous commit
   git revert HEAD~1

   # revert last three commits
   git revert HEAD~3..HEAD

   # revert specific commit
   git revert 34abe9

   git stash           # stash your changes to store them temporarily
   git stash pop       # apply the stashed changes to start over from there

Branches basics
===============

Create and navigate
-------------------

.. code:: bash

   # create a new branch with name mybranch
   git branch mybranch

   # move to (checkout) that branch
   git checkout mybranch

   # create the branch then checkout
   git checkout -b mybranch

   # list branches
   git branch         # local branches only
   git branch --all   # include remote branches

Merges
------

All the following merges are to be made while being checkout out at the
receiving branch.

.. code:: bash

   # default merge instruction, fast-forward if possible, other wise
   # merge commit
   git merge mybranch

   # fast forward only (will abandon if not possible)
   git merge mybranch --ff-only

   # no fast-forward, which means a merge commit
   git merge mybranch --no-ff

   # squash merge – not to use in general
   git merge mybranch --squash

Synchronise
-----------

.. code:: bash

   # ping the remote to list all changes available
   git fetch --all

   # pull the changes available (fast-forward if possible) for the current branch
   git pull

   # push the local commits to the remote branch
   git push
