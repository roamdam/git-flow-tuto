
.. _general presentation:

A non-technical presentation of Git
===================================

Your code's diary
-----------------

Strictly speaking, Git is a *distributed version control system* (DVCS), which means, a set of tools organised for a clear purpose : give you control over the state your code is at, past and present. That's for *VCS*. We're not interested for now in the *distribution* part, you can read some details about it `here`_ !

You can think of Git as a *high-end collective diary for your code*. It enables you to save and comment on your progression at any time, in a very detailed way if you want to, or in a bulky way if that's your preference. Just as you would read past pages of your diary to remember how you felt about those holidays last summer, you can tell Git to bring back an old version of your code, in case your recent work has broken something, for instance, and start over from there.

You can use it to work on ideas, sketch things, and decide later if you want to integrate those ideas to your code and how. You can even truly experiment on crazy ideas without affecting the existing, note things about it, decide if it is worth adding to the existing, show it to people to hear their opinion about it, maybe change some things here and there... The fun part comes with the *collective* in it. When you work alone, it is not that hard to know what has happened and what is happening. As soon as you collaborate with others on the same repository, knowing how to use this diary becomes harder yet critical.

Tools, knowledge and rules
--------------------------

Writing a diary requires several things : the diary itself, one or more pens. It also requires that you know how to write, so you and others can read and understand your work. When you get comfortable with writing, you start to use more advanced tools : different colors and stylings, bookmarks, etc. Lastly, you implicitely or explicit set some rules : a title starts on the top, write today's date on the top-right corner, do not use more than one page for a single day, etc.

This tutorial will guide through the exact same things applied to **using Git to collaborate on code**. If you are new to it, you first need to get acquainted with the basic tools (I'll say *commands* from now on) that everything will be built upon. This you'll learn in the following section :ref:`first steps a` and :ref:`first steps b`.

At this point, you'll be ready to learn how to collaborate with others on a repository. Collaborating – except if you're looking for chaos – requires that you settle on a shared set of rules, meant to be abide by everyone. Those rules are often ideals, that are difficult to follow every time you code. Just remember that they often come from people that have already handled the exact same problems as the ones you'll face one day, and that those rules intend to ease them. So trust them ! We'll present in section :ref:`git flow`, a workflow that is not the simplest but has proved to work in many situations : *git flow*.

Once you know the basics and the theory of collaboration with git flow, you'll be presented with other Git tools that tremendously help with maintaining a clean and usable history of your code. Those tools need some more attention, since they can erase things and change history : you don't really want to mess up with time ! But with Git, actually you can – sometimes. More on that in :ref:`collaboration` section.

Combine Git bash with a cool Git client
---------------------------------------

To use Git, you enter commands that do stuff : save, comment, go back in time, merge different versions of your code, etc. The first way to do that – the one you *should* start with, is using Git bash or a terminal window (if you're working on Linux or MacOS). Writing instructions is easy there, but it is far less convenient to explore and visualise the history and changes within your code.

To do so, you can use a *graphical user interface* (GUI), also referred to as *Git clients*. These are softwares that allow you not only to visualise and explore the git tree, but also to run most of the git commands without having to type them in a terminal window. You'll often read that it is best to learn how to use git bash before using a GUI, because otherwise you may end up dependant of your GUI : it prevents you from truly knowing how to talk to git or how to handle difficult cases that the GUI is not built for.

GUI are of no help when it comes to using virtual machines : since, in most cases, you communicate with them from a terminal window, you *need* to know raw git commands. I thus totally agree with the idea that you should do the whole tutorial from Git bash or a terminal window. However, I also strongly recommand that, in the meantime, you download a GUI of your choice, *only to visualise what the commands do and the state of the git history*. A git tree can easily be messy and intricate, and Git built-in visualisation commands are not friendly for complex cases.

None of the exercices below require a GUI, but having it along the way will undoubtedly help you with knowing what you're doing and remembering what are the various commands for. A widely use git client is `sourcetree`_. I personnaly use `GitKraken`_, without any strong reason to except that it has a cool name and a dark theme. Both of them are free (at least for open source packages with GitKraken). All screenshots that you'll see here are taken from this repository's tree, visualised with GitKraken. You can find a list of GUI on `Git official website`_.

.. _here: https://opensource.com/article/17/12/fork-clone-difference
.. _sourcetree: https://www.sourcetreeapp.com/
.. _GitKraken: https://www.gitkraken.com/
.. _Git official website: https://git-scm.com/downloads/guis
