
.. include:: ../../README.rst

.. toctree::
   :maxdepth: 2
   :caption: Tutorial

   introduction
   1-a-first_steps
   1-b-flow
   2-gitflow
   3-collaboration

.. toctree::
   :maxdepth: 1
   :caption: Commands summary

   commands-1
