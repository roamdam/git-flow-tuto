Git and Gitflow tutorial
========================

|  *This page is only the README landing page for the repository*, *head over to*
|  https://git-flow-tutorial.readthedocs.io/en/latest/ *for the tutorial itself*

Welcome ! This repository contains a tutorial to learn how to use Git and Git flow to collaborate on code with others. Should you be a total newbie to git, you'll find (almost) everything you need to learn the basics and become autonomous on a small project. You may already be familiar with most git commands, but are not satisfied with the way you use it, because you do not really know how to collaborate effectively, what are the best practices, or you *feel* that you're not making the most of the branching system.

In both cases, this tutorial will help you see Git as a helpful coding partner, rather than that painful bash-thing that your team and teachers make you use without really explaining to you its principles, power and versatility. This is not a **Learn Git in five minutes** tutorial, rather something in between a single page tutorial and a small book.

What you will find here
-----------------------

If you’re totally new to Git, read the `general introduction`_ ! It is sure rather long, but it explains you what Git is without any technical considerations, so that you can start the tutorial with an already clearer idea of the big picture. I remember the first time I was presented with Git : everyone was talking about *do you commits*, *use branches*, *Git is super powerful* … Since no one really knew what it was for and how to use it the right way, it soon became an evil tool that was only repeatedly breaking my code with awful ``<<<<<<< HEAD`` lines, and that’s it !

So take the time to understand Git without having to think about any terminal window, it will save you tons of time and energy wasted speaking a language that you know the words but don’t understand yet.

Contents
--------

The first section is all about the basic commands that you would learn during your first encoutering with Git in any other tutorial. It is crucial since they are the ones that you’ll use tons of time and that should become reflexes. If you’ve already been using Git for at least one or two months, you can go fast on that one, cherry-pick the
subsections that are new to you, or go straight into the second section, about branching models and collaborating. 

From then on, rather than giving you an unordered list of things you  need to know, the tutorial is built so as to reproduce the usual steps of code development in a team, to accompany you from the writer’s block to succesful collaboration and conflicts resolution with your team.

Do code while reading
---------------------

You can obviously read that tutorial without coding, but you’re likely to forget almost everything when you’re done. Feel free to use a personal repository to apply stuff you’ll read here, or to clone the `repository`_  associated with these tutorial to have at hand an exemple of a repository following (most of the time) the ideas and principles you’ll hear about here.

There’s a little python script in it that makes a good material to train on. You can download and install python `here`_. Do not fear to break things : this repository is protected from anyone but me trying to modify existing things. So please play with the instructions and do break things, for it is the best way to learn, and you’ll always be able to delete everything and clone the tutorial again, or simply go back to before you broke something (that’s precisely the thing about Git). If you don't know python, just use any type of text file you want to use as food for git.

----------------------

Contributing
------------

I welcome suggestions through merge requests ! You'll see that the little code used to give examples in the tutorial uses `pyux`_ package, which I did too because I love that kind of little but helpful packages. Feel free to check it out. Comments and merge requests are welcome here too.

About me
--------

I'm a data scientist, not a real developer, and not a pro git user at all. But I like git, and I like to explain things, so here it is. I think not being a pro on the matter is a good thing to the purpose of this tutorial : make someone new to git and a little bit lost able to use it in a proper yet simple way. I'll try to keep the explanations user-oriented and not too technical, which you probably don't need.

.. _repository: https://gitlab.com/roamdam/git-flow-tuto
.. _general introduction: https://git-flow-tutorial.readthedocs.io/en/latest/introduction.html
.. _here: https://www.python.org/downloads/
.. _pyux: https://pypi.org/project/pyux-track/
