
from argparse import ArgumentParser
from pyux.console import Wheel
from pyux.console import ColourPen
from random import choice
from time import sleep


def parse_args():
    """Prepare command line arguments"""
    parser = ArgumentParser()
    parser.add_argument('-l', '--length', type = int, default = 30, help = 'Number of iterations')
    parser.add_argument('-t', '--time', type = float, default = 0.1, help = 'Duration of each iteration (in secs)')
    parser.add_argument('-r', '--rainbow', action = 'store_true', help = 'Use rainbow wheel')
    return parser.parse_args()


def main(args):
    pen = ColourPen()
    
    # Prepare message according to rainbow argument
    if args.rainbow:
        happy_message = ["She's like a rainbow" for _ in range(args.length)]
    else:
        happy_message = ["He's the man who sold the world" for _ in range(args.length)]
        
    # Print wheel, rainbow it if rainbow flag was given
    for _ in Wheel(happy_message, print_value = True):
        sleep(args.time)
        if args.rainbow:
            pen.write(color = choice(pen.__colors__))

    # Closing message
    pen.write(
        message = 'This excellent program has ended.',
        color = 'cyan', style = 'bright',
        reset = True, newline = True
    )
    

if __name__ == '__main__':
    main(args = parse_args())
